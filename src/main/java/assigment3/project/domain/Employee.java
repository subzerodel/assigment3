package assigment3.project.domain;


public class Employee extends User {
    private String action;

    public Employee() {

    }

    public Employee(String name, String surname, String username, String password) {
        super(name, surname,username,password);
        setAction(action);

    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @Override
    public String toString() {
        return super.toString() + " " + action;
    }

}

