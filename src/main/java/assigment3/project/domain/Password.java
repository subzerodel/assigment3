package assigment3.project.domain;

public class Password {
    // passwordStr // it should contain uppercase and lowercase letters and digits
    // and its length must be more than 9 symbols
    private String password;

    public Password(String password) {
        setPassword(password);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        if (checkPassword(password)) {
            this.password = password;
        } else {
            System.out.println("Incorrect password");
        }
    }

    public static boolean checkPassword(String password){
//        boolean k = false;
//        if( password.length() < 9){
//            System.out.println("Wrong. Min length is 9.");
//            return false;
 //       }
 //       if( password == password.toLowerCase()|| password ==  password.toUpperCase()){
 //           System.out.println("Wrong. Include at least one lowercase and uppercase letter.");
  //          return false;
 //       }
  //      for( char i:  password.toCharArray()){
   //             k= true;}
  //      }
   //     if(k!=true){
  //          System.out.println("Wrong. Include at least 1 digit.");
  //          return false;
 //       }
  //      k = false;
 //       return k;
        char currentChar;
        boolean number = false;
        boolean upperCase = false;
        boolean lowerCase = false;


        if (password.length() >= 9) {
            for (int i = 0; i < password.length(); i++) {
                currentChar = password.charAt(i);
                if (currentChar >= 'A' && currentChar <= 'Z') {
                    upperCase = true;
                } else if (currentChar >= 'a' && currentChar <= 'z') {
                    lowerCase = true;
                } else if (currentChar >= '0' && currentChar <= '9') {
                    number = true;
                }
            }
            return number && upperCase && lowerCase;
        } else {
            return false;
        }


}

    @Override
    public String toString() {
        return password;
    }
}
