package assigment3.project.domain;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class MyApplication {
    // users - a list of users
    private Scanner sc = new Scanner(System.in);
    private User signedUser;
    private List<User> userLinkedList;

    public MyApplication() throws FileNotFoundException {
        userLinkedList = new LinkedList<User>();
        fillUsers();
    }

    private void fillUsers() throws FileNotFoundException {
        File file = new File("C:\\Users\\Самал\\IdeaProjects\\Assigment3\\src\\main\\java\\assigment3\\project\\domain\\db.txt");
        Scanner fsc = new Scanner(file);
        while (fsc.hasNext()) {
            userLinkedList.add(new User(fsc.nextInt(), fsc.next(), fsc.next(), fsc.next(), fsc.next()));
        }
    }


    private void addUser(User user) {
        userLinkedList.add(user);
    }

    private void menu() {
        while (true) {
            if (signedUser == null) {
                System.out.println("You are not signed in.");
                System.out.println("1. Authentication");
                System.out.println("2. Exit");
                int choice = sc.nextInt();
                if (choice == 1) authentication();
                else break;
            } else {
                userProfile();
            }
        }
    }

    private void userProfile() {
        while (true) {
            System.out.println("Hello " + signedUser.getName() + " " + signedUser.getSurname());
            System.out.println("1) Log off");
            int choice = sc.nextInt();
            if (choice == 1) {
                logOff();
                break;
            }
        }
    }

    private void logOff() {
        signedUser = null;
    }

    private void authentication() {
        // sign in
        // sign up
        while (true) {
            System.out.println("1.Sign in");
            System.out.println("2.Sign up");
            int choice = sc.nextInt();
            if (choice == 1) {
                signIn();
            } else {
                signUp();
            }

        }
    }

    private void signIn() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("username: ");
        String username = scanner.next();
        System.out.println("password: ");
        String password = scanner.next();
        Password password1 = new Password(password);
        for (User user : userLinkedList) {
            if (username == user.getUsername() && password1 == user.getPassword()) {
                signedUser = user;
            }
        }

    }

    private void signUp() {

        Scanner scanner = new Scanner(System.in);
        System.out.println("please enter name");
        String name = scanner.next();
        System.out.println("please enter surname");
        String surname = scanner.next();
        System.out.println("please enter username");
        String username = scanner.next();
        System.out.println("please enter password");
        String password = scanner.next();


        if (password != null && username != null && name != null && surname != null) {
            User user = new User();
            user.setName(name);
            user.setSurname(surname);
            user.setUsername(username);
            user.setPassword(password);

            if (user.getPassword() != null) {
                addUser(user);
                signedUser = user;
            }
        }

    }

    public void start(){

        // fill userLinkedList from db.txt

        while (true) {
            System.out.println("Welcome to my application!");
            System.out.println("Select command:");
            System.out.println("1. Menu");
            System.out.println("2. Exit");
            int choice = sc.nextInt();
            if (choice == 1) {
                menu();
            } else {
                break;
            }
        }


        // save the userLinkedList to db.txt
    }




    private void saveUserList() throws IOException {
        String data = "";
        for (User user : userLinkedList) {
            data += user.getId() + " " + user.getName() + " " + user.getSurname() + " " +
                    user.getUsername() + " " + user.getPassword() + "\n";
            }

        Files.write(Paths.get("C:\\Users\\Самал\\IdeaProjects\\Assigment3\\src\\main\\java\\assigment3\\project\\domain\\db.txt"), data.getBytes());

        }

    }
